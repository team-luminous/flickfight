﻿#pragma strict



var rightWing = 150;
var leftWing = 150;
var engine = 150;
var missileLauncher = 150.0;
var laser = 150.0;


var engineCurrent = 0.0;
var lastSpeed = 0.0;



function FixedUpdate() {
	if(Mathf.Abs(gameObject.GetComponent(Rigidbody).velocity.magnitude-lastSpeed)>1) {
		engine-=Mathf.Abs(gameObject.GetComponent(Rigidbody).velocity.magnitude-lastSpeed)*Mathf.Abs(engineCurrent)/300;
		missileLauncher-=Mathf.Abs(gameObject.GetComponent(Rigidbody).velocity.magnitude-lastSpeed)*Mathf.Abs(engineCurrent)/300;
	
	}
	lastSpeed=gameObject.GetComponent(Rigidbody).velocity.magnitude;
	if (rightWing<=0) {
		rightWing=0;
		transform.Find("RotationFix").Find("Mesh").Find("rightWing").gameObject.SetActiveRecursively(false);
		transform.Find("RotationFix").Find("Mesh").Find("mesh3_001").gameObject.SetActiveRecursively(false);
		transform.Find("RotationFix").Find("Colliders").Find("Right Wing").gameObject.SetActiveRecursively(false);
		transform.Find("RotationFix").Find("Thrusters").Find("MainRight").gameObject.SetActiveRecursively(false);
	}
	if (leftWing<=0) {
		leftWing=0;
		transform.Find("RotationFix").Find("Mesh").Find("leftWing").gameObject.SetActiveRecursively(false);
		transform.Find("RotationFix").Find("Mesh").Find("mesh2_001").gameObject.SetActiveRecursively(false);
		transform.Find("RotationFix").Find("Colliders").Find("Left Wing").gameObject.SetActiveRecursively(false);
		transform.Find("RotationFix").Find("Thrusters").Find("MainLeft").gameObject.SetActiveRecursively(false);
	}
	if (engine<0) engine=0;
	if (missileLauncher<1) missileLauncher=1;
	if (laser<1) laser=1;
}

function exploded (event, source : Vector3){
if (event=="missile") {
	rightWing-=(110-(source-transform.position).magnitude)/3;
	leftWing-=(110-(source-transform.position).magnitude)/3;
	engine-=(100-(source-transform.position).magnitude)/3;
}
}

function OnCollisionEnter (other : Collision) {

		for (var contact : ContactPoint in other.contacts) {
			if (contact.thisCollider.name=="Cabine"){
				missileLauncher-=other.relativeVelocity.magnitude/50;
				laser-=other.relativeVelocity.magnitude/30;
			} else if (contact.thisCollider.name=="Back"){
				engine-=other.relativeVelocity.magnitude/70;
				Debug.Log("Back Hit");
			} else if (contact.thisCollider.name=="Left Wing"){
				rightWing-=other.relativeVelocity.magnitude/30;
				Debug.Log("Left Wing Hit");
			} else if (contact.thisCollider.name=="Right Wing"){
				leftWing-=other.relativeVelocity.magnitude/30;
				Debug.Log("Right Wing Hit");
			}

		}


}