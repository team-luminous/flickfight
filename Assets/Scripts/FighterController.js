﻿#pragma strict
var Projectile : Transform;
var gunLeft = true;
var radius : float = 50;
var power : float = 1000;
var acceleration = 0.0;
var UpDown = 0.0;
var LeftRight = 0.0;
var Roll = 0.0;

var MaxPower : float = 15000;

var lastShot = 0f;

function dest(laser){
	yield WaitForSeconds (0.3);
	Destroy(laser);
}

function Controll(action, Float : float) {
	transform.Find("RotationFix").Find("Thrusters").Find("Booster").Find("Particle System").GetComponent(ParticleSystem).emissionRate=acceleration*30.0;
	transform.Find("RotationFix").Find("Thrusters").Find("MainLeft").Find("Particle System").GetComponent(ParticleSystem).emissionRate=(acceleration+1*50.0)+LeftRight*3;
	transform.Find("RotationFix").Find("Thrusters").Find("MainRight").Find("Particle System").GetComponent(ParticleSystem).emissionRate=(acceleration+1*50.0)-LeftRight*3;
	if (action=="accelerate") {
		acceleration += Float * 2;
		gameObject.GetComponent(Health).engineCurrent=acceleration;
		acceleration = Mathf.Min(acceleration, 100);
		acceleration = Mathf.Max(acceleration, -10);
		rigidbody.AddForce(transform.forward * MaxPower * acceleration * (gameObject.GetComponent(Health).engine/150.0) / 30);
	} else if (action=="upDown") {
	
		//Debug.Log(UpDown + "+"+ Float + " * 4 =");
		UpDown += Float *4;
		UpDown = Mathf.Min(150, UpDown);
		UpDown = Mathf.Max(-150, UpDown);
		//Debug.Log(UpDown);
		var percentage = (UpDown * UpDown * UpDown) / 50000;
		rigidbody.AddTorque(-transform.right * MaxPower * percentage * 1);

	} else if (action=="turn") {
		
		Roll /= 2;
		LeftRight += Float * 3;
		LeftRight = Mathf.Min(gameObject.GetComponent(Health).leftWing, LeftRight);
		LeftRight = Mathf.Max(gameObject.GetComponent(Health).rightWing*-1, LeftRight);
		percentage = (LeftRight * LeftRight * LeftRight) / 50000;
		rigidbody.AddTorque(transform.up * MaxPower * percentage);
	}
	else if ( action == "roll")
	{
		var rollSign=1;
		if (Roll<0) rollSign=-1;
		Roll += Float * 3;
		Roll = Mathf.Min(gameObject.GetComponent(Health).leftWing, Roll);
		Roll = Mathf.Max(gameObject.GetComponent(Health).rightWing*-1, Roll);
		percentage = (Roll * Roll) / 700 * rollSign;
		rigidbody.AddTorque(-transform.forward * MaxPower * percentage);
	}	
	else if (action=="missile") {
		if(Time.time - lastShot > 200/gameObject.GetComponent(Health).missileLauncher)
		{
			var Pos =  transform.position;
			var direction = transform.rotation;
			Pos+=transform.forward*14;
			var CurrentProjectile = Instantiate (Projectile,  Pos, direction).gameObject;
			CurrentProjectile.GetComponent(Rigidbody).velocity = rigidbody.velocity * 1;
			lastShot = Time.time;
		}
	}


}