﻿#pragma strict

var radius : float;
var power : float;
var particles : GameObject;
var target : GameObject;

function Start()
{
	var potensialTarget = Physics.OverlapSphere(transform.position, 1000);
	var bestTarget : GameObject;
	var bestDistance = float.MaxValue;
	for (var i = 0; i < potensialTarget.length; i++) {
		if(potensialTarget[i].transform.name == "Cabine")
		{
			if(Vector3.Dot(transform.forward, (potensialTarget[i].transform.position - transform.position).normalized) > 0.5)
			{
				if((potensialTarget[i].transform.position - transform.position).magnitude < bestDistance)
				{
					bestTarget = potensialTarget[i].gameObject;
					bestDistance = (potensialTarget[i].transform.position - transform.position).magnitude;
				}
			}
		}
	}
	target = bestTarget;
}

function Update()
{
	if(target)
	{
		rigidbody.AddTorque(-Vector3.Cross(target.transform.position - transform.position, transform.forward));
	}
}

function OnTriggerExit (other : Collider) {

 Destroy(gameObject);
 Debug.Log(other.transform.name);
 var explosionPos : Vector3 = transform.position;
 var colliders : Collider[] = Physics.OverlapSphere (explosionPos, radius);
for (var i = 0; i < colliders.length; i++) {
	if (colliders[i].gameObject.transform.name=="Cabine"){
		colliders[i].transform.parent.parent.parent.GetComponent(Health).exploded("missile", transform.position);
	}
}
 for (var hit : Collider in colliders) {
 if (!hit)
  continue;
			
 if (hit.rigidbody)
  hit.rigidbody.AddExplosionForce(power, explosionPos, radius, 3.0);
 }

}

function OnCollisionEnter (other : Collision) {

 Debug.Log(other.transform.name);
 var explosionPos : Vector3 = transform.position;
 var colliders : Collider[] = Physics.OverlapSphere (explosionPos, radius);


for (var i = 0; i < colliders.length; i++) {
	Debug.Log(i);
	if (colliders[i].gameObject.transform.name=="Cabine"){
		colliders[i].transform.parent.parent.parent.GetComponent(Health).exploded("missile", transform.position);
	}
}
 for (var hit : Collider in colliders) {
 if (!hit)
  continue;
			
 if (hit.rigidbody)
  hit.rigidbody.AddExplosionForce(power, explosionPos, radius, 3.0);
 }
 GameObject.Instantiate(particles, transform.position, Quaternion.identity);
 Destroy(gameObject);

}




