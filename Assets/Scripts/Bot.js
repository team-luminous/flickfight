﻿#pragma strict
class Bot extends FighterController{

	var target : Collider;
	var state = "Attack";
	var targetpos :Vector3;

	function controlUpDownRotation(angle : float){
		var myUpDown = angle * 500;
		//Debug.Log(UpDown + ";" + myUpDown + ";" + angle + ";" + d);
		Controll("upDown", (myUpDown - UpDown) / 4 );
	}

	function controlTurn(angle : float){
		var myTurn = angle * -500;
		Controll("turn", (myTurn - LeftRight) / 3 );
	}


	function MovementControl (target : Vector3) {
		var VtoTarget = (target - transform.position).normalized;
		/*var cross = Vector3.Cross(VtoTarget, transform.forward);
		//Debug.Log(cross.x);
		cross.x = Mathf.Min(1, cross.x);
		cross.x = Mathf.Max(-1, cross.x);
		var angle = -Mathf.Asin(cross.x);
		//Debug.Log("a"+angle);
		//Debug.Log("v"+Vector3.Dot(transform.forward,VtoTarget));
		if (Vector3.Dot(transform.forward,VtoTarget) < 0 && Mathf.Abs(angle) < 0.5)
		{
			if(angle > 0) angle = 1 - angle;
			else angle = -1 - angle;
			rigidbody.AddTorque(-Vector3.Cross(target - transform.position, transform.forward)*(150*150/50000*MaxPower));
		}
		//Debug.Log("b"+angle);
		else {controlUpDownRotation(angle);
			//Debug.Log("u"+UpDown);
			if(Mathf.Abs(angle) < 0.5)
			{
				
				cross.y = Mathf.Min(1, cross.y);
				cross.y = Mathf.Max(-1, cross.y);
				angle = Mathf.Asin(cross.y);
				if (Vector3.Dot(transform.forward,VtoTarget) < 0&& Mathf.Abs(angle) < 0.5)
				{
					if(angle > 0) angle = 1 - angle;
					else angle = -1 - angle;
					rigidbody.AddTorque(-Vector3.Cross(target - transform.position, transform.forward)*(150*150/50000*MaxPower));
				}
				else controlTurn(angle);
			}
		}*/
		rigidbody.AddTorque(-Vector3.Cross(target - transform.position, transform.forward)*(150*150*10/50000*MaxPower)*Random.value);
		
		if(Vector3.Dot(VtoTarget, transform.forward) > 0.4)
		{
			Controll("accelerate", 150);
		} //else Debug.Log(Vector3.Dot(VtoTarget, transform.forward));
	}
		

	function FixedUpdate()
	{
		var healthSum = gameObject.GetComponent(Health). laser + gameObject.GetComponent(Health). rightWing + gameObject.GetComponent(Health). leftWing + gameObject.GetComponent(Health). engine + gameObject.GetComponent(Health). missileLauncher;
		if (healthSum < 100) Destroy(gameObject);
		if (Time.time%10==0) target = (Random.value<0.5) ? null  : target;
		if(!target)	{
			var potensialTarget = Physics.OverlapSphere(transform.position, 1000);
			for (var i = 0; i < potensialTarget.length; i++) {
				if (potensialTarget[i].gameObject.transform.name=="Cabine"){
					if (target && (transform.position-potensialTarget[i].gameObject.transform.position).magnitude <
						(transform.position-target.gameObject.transform.position).magnitude && 
						(transform.position-potensialTarget[i].gameObject.transform.position).magnitude!=0){
						target = potensialTarget[i];
					} else if ((transform.position-potensialTarget[i].gameObject.transform.position).magnitude!=0) {
						target = potensialTarget[i];
					}
				}                                                          
			}
			if (!target) return;
		}
		
		switch(state)
		{
			case "Attack":
				MovementControl(target.transform.position);
				//Debug.Log(target.transform.position);
				if(Vector3.Dot((transform.position - target.transform.position).normalized, transform.forward) < -0.7 && Random.value>0.8 && Time.time%1==0) Controll("missile", 1);
				//Debug.Log((target.transform.position - transform.position).magnitude);
				if((target.transform.position - transform.position).magnitude < 50)
				{
					state = (Random.value > 0) ? "Evade" : "Flyby";
					if(state == "Evade") targetpos = transform.position -transform.forward * 150;
					else  targetpos = transform.TransformPoint(2 * target.transform.position + transform.position) * 2 + Random.insideUnitSphere * 30;
				}
				break;
			case "Evade":
				if(Vector3.Dot((transform.position - target.transform.position).normalized, target.transform.forward) > 0.5) state = (Random.value > 0.9) ? "Evade" : "Attack";
				if ((transform.position - target.transform.position).magnitude>250) state = "Attack";
				targetpos = transform.position+(transform.position - target.transform.position);
				MovementControl(targetpos);
				break;
			case "Flyby":
				MovementControl(targetpos);
				if((transform.position - targetpos).magnitude <= 25 && Random.value>0.95)
				{
					targetpos = target.transform.position;
					state = "Attack";
				}
				break;
			default:
				target = null;
				break;
		}
	}
}