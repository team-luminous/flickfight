﻿#pragma strict

var Shield : GameObject;

function OnTriggerExit (other : Collider) {
var odirection = other.transform.position.normalized;
var forceposition = odirection * GetComponent(SphereCollider).radius;
var sh = GameObject.Instantiate(Shield, forceposition, Quaternion.identity);
sh.transform.forward = odirection;
var otransform = other.transform;
while(otransform.rigidbody == null)
	otransform = otransform.parent;
 //Debug.Log(otransform.rigidbody.velocity.magnitude);
 //Debug.Log((otransform.position*-1).normalized*otransform.rigidbody.velocity.magnitude);
 otransform.rigidbody.velocity=(otransform.position*-1).normalized*otransform.rigidbody.velocity.magnitude;
//Debug.Log("Bounce: " + otransform.name);
}