﻿#pragma strict
class Shoot extends FighterController{
var Gui : GameObject;
var ScrollWheel : float = 0;
	var beginTime=Time.time;

function Start() {
	Screen.lockCursor=true;
}

function OnGUI(){
	GUI.Label(Rect (10,10,200,20),"rightWing: "+(gameObject.GetComponent(Health).rightWing/150.0*100)+"%");
	GUI.Label(Rect (10,30,200,20),"leftWing: "+(gameObject.GetComponent(Health).leftWing/150.0*100)+"%");
	GUI.Label(Rect (10,50,200,20),"engine: "+(gameObject.GetComponent(Health).engine/150.0*100)+"%");
	GUI.Label(Rect (10,70,200,20),"missileLauncher: "+(gameObject.GetComponent(Health).missileLauncher/150.0*100)+"%");
	GUI.Label(Rect (10,90,200,20),"laser: "+(gameObject.GetComponent(Health).laser/150.0*100)+"%");
}

function FixedUpdate () {
		var healthSum = gameObject.GetComponent(Health).laser + gameObject.GetComponent(Health).rightWing + gameObject.GetComponent(Health).leftWing + gameObject.GetComponent(Health).engine + gameObject.GetComponent(Health).missileLauncher;
		if (healthSum < 100) {
			Gui.GetComponent(MainMenu).dead = true;
			Gui.GetComponent(MainMenu).score = Time.time-beginTime;
			Screen.lockCursor=false;
			Gui.SetActiveRecursively(true);
			Time.timeScale=0;
		}
	if (Input.GetKey("escape")){
		Screen.lockCursor=false;
		Gui.SetActiveRecursively(true);
		Time.timeScale=0;
	}
	ScrollWheel = Input.GetAxis("Mouse ScrollWheel");
	if (Input.GetButtonDown ("Fire1")) {
		Controll("missile", 0);
	}
	if (Input.GetButtonDown ("Fire2")) {
		Controll("laser", 0);
	}/*
	var camPos=new Vector3(0, 6.5, -17);
	camPos.x+=LeftRight/-50;
	camPos.x+=Roll/-50;
	camPos.y+=UpDown/30;
	camPos.z+=acceleration/-30;
	gameObject.Find("Camera(Clone)").transform.localPosition= camPos;
	var camRot = Quaternion.LookRotation(Vector3(0,0,0),transform.up);
	camRot.x+=UpDown/700;
	camRot.y+=LeftRight/-700;
	camRot.z+=Roll/500;
	gameObject.Find("Camera(Clone)").transform.localRotation=camRot;*/

	Controll("accelerate", ScrollWheel);
	Controll("upDown", Input.GetAxis("Mouse Y"));
	if(Input.GetButton("Roll"))
	{
		Controll("roll", Input.GetAxis("Mouse X"));
	}
	else
	{
		Controll("turn", Input.GetAxis("Mouse X"));
	}
}









}