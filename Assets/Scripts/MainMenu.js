﻿#pragma strict
var mainCam : GameObject;
var gray : Texture2D;
var button : Texture2D;
var noBorder : GUIStyle;
var noBorderButton : GUIStyle;
var bck : Texture2D;
var main : System.Boolean;
var score : int;
var dead : System.Boolean;
var res : GameObject[];
var TextStyle : GUIStyle;

function OnGUI () {

	GUI.Box (Rect (0, 0, Screen.width,Screen.height), GUIContent(gray), noBorder);
	GUI.DrawTexture(Rect(0,0,Screen.width,Screen.height), bck, ScaleMode.ScaleToFit, true);
	if(dead){
		GUI.Label(Rect (Screen.width/2-100,Screen.height/3*2-70, 200, 40), "CONNECTION LOST; SCORE: " + score, TextStyle);
		if(Camera.main)
			Camera.main.enabled = false;
	}
	if(!dead)
	{
	if (GUI.Button (Rect (Screen.width/2-70,Screen.height/3*2-20, 140, 20), "Return to Game", noBorderButton)) {
		Screen.lockCursor=true;
		gameObject.SetActiveRecursively(false);
		Time.timeScale=1;
	}
	}
	if (GUI.Button (Rect (Screen.width/2-70,Screen.height/3*2+10, 140, 20), "Restart Game", noBorderButton)) {
		var allObjects = FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[];
		for(var i = 0; i < allObjects.Length; i++)
		{
			if (allObjects[i].activeInHierarchy)
				if(allObjects[i] != gameObject)
					DestroyImmediate(allObjects[i]);
		}
		if(res)
		{
			for(i = 0; i < res.Length; i++)
			{
					var cur = GameObject.Instantiate(res[i]);
					cur.transform.name = cur.transform.name.Substring(0, cur.transform.name.Length - 7);
			}
			res[2].GetComponent(GameWorldGenerator).Gui = gameObject;
		}
		else
		{
			Debug.Log("ERROR");
		}
	}
	if (GUI.Button (Rect (Screen.width/2-70,Screen.height/3*2+40, 140, 20), "Return to Desktop", noBorderButton)) {
		Application.Quit();
	}
}