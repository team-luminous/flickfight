﻿#pragma strict


var Seed : int ;
var WorldSize : int ;
var GameArea : GameObject;
var FirstPerson : Transform;
var Projectile : Transform;
var MainCamera : Transform;
var Gui : GameObject;

function Awake () {
	GameArea.GetComponent(SphereCollider).radius = WorldSize;
	Random.seed=Seed;
	Debug.Log("worldgeneration");
	
	var person = Instantiate (FirstPerson).gameObject;
	var MainCamera = GameObject.Find("Camera");
	MainCamera.GetComponent(CameraMove).target = person.transform;
	/*var MainCamera = Instantiate (MainCamera).gameObject;
	MainCamera.transform.position= new Vector3(0, 0, 10);
	MainCamera.transform.parent=person.transform;
	MainCamera.transform.Rotate(14, 0, 0);*/
	var shoot : Shoot;
	shoot = person.AddComponent(Shoot);
	shoot.Projectile = Projectile;
	shoot.Gui = Gui;
	

	//var enemy = Instantiate (FirstPerson, Vector3(0,5,150), Quaternion.LookRotation(Vector3(0,0,1))).gameObject;
	for (var e = 0; e < 9; e++){
	var enemy = Instantiate (FirstPerson, Random.insideUnitSphere*WorldSize, Random.rotation).gameObject;
	var bot : Bot;
	bot = enemy.AddComponent(Bot);
	bot.Projectile = Projectile;
	
	}
	
	
	
	
	
	


	var bigTrash : Object[] = Resources.LoadAll("BigTrash");
	var smallTrash : Object[] = Resources.LoadAll("SmallTrash");
	
	var smallTrashParent = new GameObject("Small Trash");
	var bigTrashParent = new GameObject("Big Trash");
	

	for (var i = 0; i < Random.value*10+10; i++) {
		var CurrentPiece = Instantiate (smallTrash[Random.Range(0, smallTrash.Length)],  Random.insideUnitSphere*WorldSize, Random.rotation) as GameObject;
		CurrentPiece.GetComponent(Rigidbody).velocity = Vector3(Random.value*3,Random.value*3,Random.value*3);
		CurrentPiece.transform.name = "Smalltrash" + i;
		CurrentPiece.transform.parent = smallTrashParent.transform;
	}
	for (var j = 0; j < Random.value*5+20; j++) {
		var CurrentPiece2 = Instantiate (bigTrash[Random.Range(0, bigTrash.Length)],  Random.insideUnitSphere*WorldSize, Random.rotation) as GameObject;
		CurrentPiece2.transform.name = "Bigtrash" + j;
		CurrentPiece2.transform.parent = bigTrashParent.transform;
	}
	/*for (var k = 0; k < Random.value*50+300; k++) {
		var CurrentPiece3 = Instantiate (bigTrash[Random.Range(0, bigTrash.Length)],  Random.onUnitSphere*(WorldSize+Random.Range(-70, 0)), Random.rotation) as GameObject;
		CurrentPiece3.transform.name = "Bigtrash" + k;
		CurrentPiece3.transform.parent = bigTrashParent.transform;
	}*/

	
}

static function RandomizeArray(arr : Array)
{
    for (var i = arr.length - 1; i > 0; i--) {
        var r = Random.Range(0,i);
        var tmp = arr[i];
        arr[i] = arr[r];
        arr[r] = tmp;
    }
}





var lastTime;
function Update () {
var time = Mathf.Floor(Time.time);
if (time%10==0 && lastTime!=time){
lastTime=time;
var alive = GameObject.FindGameObjectsWithTag("F302").length;
Debug.Log(alive);
	for (var e = 0; e < 10-alive; e++){
		Debug.Log("birth");
		var enemy = Instantiate (FirstPerson, Random.insideUnitSphere*WorldSize, Random.rotation).gameObject;
		var bot : Bot;
		bot = enemy.AddComponent(Bot);
		bot.Projectile = Projectile;
	
	}
var positions = Array();
var rotations = Array();
var velocityes=Array();
var aliveA = GameObject.FindGameObjectsWithTag("F302");
for(var fooObj : GameObject in aliveA)
{
	positions.push(fooObj.transform.position);
	rotations.push(fooObj.transform.rotation);
	velocityes.push(fooObj.rigidbody.velocity);
}
RandomizeArray(positions);
RandomizeArray(rotations);
RandomizeArray(velocityes);
for (var p = 0; p < 10; p++){
	aliveA[p].transform.position=positions[p];
	aliveA[p].transform.rotation=rotations[p];
	aliveA[p].rigidbody.velocity=velocityes[p];
}





}
}
