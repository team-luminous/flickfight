﻿using UnityEngine;
using System.Collections;

public class TorqueTest : MonoBehaviour {

	int cnt = 0;
	// Update is called -once per frame
	void FixedUpdate () {
		rigidbody.AddTorque(1, 0, 0);
		cnt++;
		Debug.Log(rigidbody.angularVelocity.magnitude + ";" + Time.time);
	}
}
