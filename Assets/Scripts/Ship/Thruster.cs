﻿using UnityEngine;
using System.Collections;

public class Thruster : MonoBehaviour {
	public Transform superparent;
	public float power = 0;
	public float maxpower = 100;

	void Start() {
		
		superparent = transform.parent;
		while(superparent.parent != null)
		{
			superparent = superparent.parent;
		}		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(power > maxpower)
			power = maxpower;
		transform.GetChild(0).GetComponent<ParticleSystem>().emissionRate = power / 50;
		superparent.rigidbody.AddForceAtPosition(transform.forward * -1 * power, transform.position);
	}
}
